
/*
 * Thomas Keady and Nitin Kumar
 * tkeady1 nkumar14
 * 516-729-9535
 * 248-797-2533
 * tkeady1@jhu.edu
 * nkumar14@jhu.edu
 * 4/1/15
 * HW5
 */

#include <string>
#include <vector>
#include <iostream>
#include <cassert>

#include "langMod.h"

using std::cin;
using std::cout;
using std::endl;

int main() {
	//begin reading input
	cout << "Print input lines here (ignore if echo inputting):" << endl;
	vector<string> inputVec = readInput();		
	//generate lang model
	LanguageModel langModel(inputVec);
	vector<string> outputVec = genText(langModel);
	//print values
	cout << "Output:" << endl;
	printVector(outputVec);
	return 0;
}
