
/*
 * Thomas Keady and Nitin Kumar
 * tkeady1 nkumar14
 * 516-729-9535
 * 248-797-2533
 * tkeady1@jhu.edu
 * nkumar14@jhu.edu
 * 4/1/15
 * HW5
 */

#include <string>
#include <vector>
#include <iostream>
#include <cassert>
//#include <assert>

#include "langMod.h"

using std::cout;
using std::endl;


/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
//bool fileeq(char lhsName[], char rhsName[]) {
bool fileeq(const char lhsName[], const char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (!lhs || !rhs) return false;

    bool match = true;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = false;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}

/*
 * Test fileeq on same file, files with same contents, files with
 * different contents and a file that doesn't exist
 */
void test_fileeq() {
    FILE* fptr = fopen("test1.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    fptr = fopen("test2.txt", "w");
    fprintf(fptr, "this\nis\na different test\n");
    fclose(fptr);

    fptr = fopen("test3.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    assert(fileeq("test1.txt", "test1.txt"));
    assert(fileeq("test2.txt", "test2.txt"));
    assert(!fileeq("test2.txt", "test1.txt"));
    assert(!fileeq("test1.txt", "test2.txt"));

}

void testReadInput() {
	FILE* readTest = fopen("readTest.txt", "w");
	fprintf(readTest, "laa dee daa\n");
	fclose(readTest);
	freopen("readTest.txt", "r", stdin);
	
	vector<string> test = readInput();
	
	vector<string> expected = {"<START>", "laa", "dee", "daa", "<END>"};
	//printf("%s %s %s %s %s\n", expected[0].c_str(), expected[1].c_str(), expected[2].c_str(), expected[3].c_str(), expected[4].c_str());
	assert(test == expected);
	
	//printVector(test);
	
}

void testIncrementOccur() {
	vector<string> testData= {"ate", "bit", "ate", "cow", "ate"};
	unordered_map <string, int> expectedMap = {{"ate" , 3}, {"bit", 1}, {"cow", 1}};
	unordered_map <string, int> resultMap;
	for(vector<string>::iterator i = testData.begin(); i != testData.end(); i++) {
		incrementOccur(resultMap, i); 
	}
	assert(expectedMap == resultMap);
}

void testIncrementPair() {
	vector<string> testData= {"ate", "bit", "ate", "bit", "cow"};
	unordered_map <string, int> expectedMap = {{"atebit" , 2}, {"bitate", 1}, {"bitcow", 1}};
	unordered_map <string, int> resultMap;
	for(vector<string>::iterator i = testData.begin(); i != testData.end(); i++) {
		incrementPair(resultMap, testData, i); 
	}
	assert(expectedMap == resultMap);
}

void testIncrementList() {
	vector<string> test = {"<START>", "laa", "dee", "daa", "<END>"};
	vector<string>::iterator start = test.begin();
	vector<string>::iterator end = test.end();
	unordered_map<string, list<string> > wordList;
	unordered_map<string, list<string> > comparer;
	
	incrementList(wordList, start, end);
	
	comparer["<START>"].push_front("laa");
	comparer["laa"].push_front("dee");
	comparer["daa"].push_front("<END>");
	assert(wordList["<START>"] == comparer["<START>"]);
	assert(wordList["laa"] == comparer["laa"]);
	assert(wordList["daa"] == comparer["daa"]);
	
	
	vector<string> test2 = {"<START>", "fa", "la", "la", "la", "la", "<END>"};
	start = test2.begin();
	end = test2.end();
	unordered_map<string, list<string> > wordList2;
	unordered_map<string, list<string> > comparer2;
	
	incrementList(wordList2, start, end);
	
	list<string> empty;
	list<string>::iterator useless = empty.begin();
	comparer2["<START>"].push_front("fa");
	comparer2["la"].push_front("la");
	comparer2["la"].push_front("<END>");
	comparer2["<END>"]; //.emplace(*useless, empty);
	comparer2["laa"].sort();
	assert(wordList2["<START>"] == comparer2["<START>"]);
	assert(wordList2["la"] == comparer2["la"]);
	assert(wordList2["<END>"] == comparer2["<END>"]);
	
	
	vector<string> test3 = {"<START>", "he", "said", "that", "she", "said", "that", "he", "said", "that", "she", "said", "that", "he", "did", "that", "<END>"};
	start = test3.begin();
	end = test3.end();
	unordered_map<string, list<string> > wordList3;
	unordered_map<string, list<string> > comparer3;
	
	incrementList(wordList3, start, end);
	
	//list<string> empty;
	//list<string>::iterator useless = empty.begin();
	comparer3["<START>"].push_front("he");
	comparer3["he"].push_front("said");
	comparer3["he"].push_front("did");
	comparer3["he"].sort();
	comparer3["said"].push_front("that");
	comparer3["that"].push_front("she");
	comparer3["that"].push_front("he");
	comparer3["that"].push_front("<END>");
	comparer3["that"].sort();
	comparer3["she"].push_front("said");
	comparer3["did"].push_front("that");
	
	comparer3["<END>"]; //.emplace(*useless, empty);
	
	assert(wordList3["<START>"] == comparer3["<START>"]);
	assert(wordList3["he"] == comparer3["he"]);
	assert(wordList3["said"] == comparer3["said"]);
	assert(wordList3["that"] == comparer3["that"]);
	assert(wordList3["she"] == comparer3["she"]);
	assert(wordList3["did"] == comparer3["did"]);
	assert(wordList3["<END>"] == comparer3["<END>"]);
	
	
}

void testMakeLangMod() {
	//Basic test case
	//Build input
	//vector<string> inputVec = {"<START>", "he", "said", "that", "<END>"};
	vector<string> inputVec = {"<START>", "the", "cat", "in", "the", "hat", "in", "the", "cat", "in", "the", "hat", "loved", "green", "eggs", "and", "ham", "<END>"};
	//Expected result

	//Build expected wordList:
	//can use increment functions to build maps
	//Because have proven that they work
	unordered_map<string, list<string> > expectedWordList;
	incrementList(expectedWordList, inputVec.begin(), inputVec.end());	
	//Build expected wordOccur:
	unordered_map<string, int> expectedWordOccur;
	unordered_map<string, int> expectedWordPair;
	for(vector<string>::iterator i = inputVec.begin(); i != inputVec.end(); i++) {
		incrementOccur(expectedWordOccur, i);	
		incrementPair(expectedWordPair, inputVec, i);	
	}
	
	//Get result
	LanguageModel langModel(inputVec);
	assert(langModel.wordList == expectedWordList);
	assert(langModel.wordOccur == expectedWordOccur);
	assert(langModel.wordPair == expectedWordPair);
	
	
}

void testGenWord() {
	//First have only one potential word to generate
	//Basic test case to make sure function works
	//string testWord = "<START>";
	
	vector<string> first = {"<START>", "U", "S", "A", "U", "S", "A", "U", "S", "A", "<END>"};
	
	LanguageModel langMod(first);
	
	assert("U" == genWord(langMod, "<START>", langMod.wordOccur["<START>"]));
	assert("U" == genWord(langMod, "<START>", langMod.wordOccur["<START>"]));
	assert("U" == genWord(langMod, "<START>", langMod.wordOccur["<START>"]));
	
	assert("S" == genWord(langMod, "U", langMod.wordOccur["U"])); // Running 3 of each since it
	assert("S" == genWord(langMod, "U", langMod.wordOccur["U"])); // makes it extremely unlikely
	assert("S" == genWord(langMod, "U", langMod.wordOccur["U"])); // that an error gets by
	
	assert("A" == genWord(langMod, "S", langMod.wordOccur["S"]));
	assert("A" == genWord(langMod, "S", langMod.wordOccur["S"]));
	assert("A" == genWord(langMod, "S", langMod.wordOccur["S"]));
	
	assert("<END>" == genWord(langMod, "A", langMod.wordOccur["<END>"]));
	assert("<END>" == genWord(langMod, "A", langMod.wordOccur["<END>"]));
	assert("<END>" == genWord(langMod, "A", langMod.wordOccur["<END>"]));
	
	vector<string> second = {"<START>", "a", "a", "b", "c", "d", "<END>"};
	
	LanguageModel langMod2(second);
	
	assert("a" == genWord(langMod2, "<START>", langMod2.wordOccur["<START>"]));
	string generatedWord = genWord(langMod2, "a", langMod2.wordOccur["a"]);
	assert(("a" == generatedWord) || ("b" == generatedWord));
	assert("c" == genWord(langMod2, "b", langMod2.wordOccur["b"]));
	assert("d" == genWord(langMod2, "c", langMod2.wordOccur["c"]));
	assert("<END>" == genWord(langMod2, "d", langMod2.wordOccur["d"]));
	
}

void testGenText() {
	
	vector<string> spongebob = {"<START>", "why", "once", "i", "met", "this", "guy", "who", "knew", "this", "guy", "who", "knew", "this", "guy", "s", "cousin", "<END>"};
	
	LanguageModel langMod3(spongebob);
	
	vector<string> returned = genText(langMod3);
	cout << returned[6] << endl;
	
	assert(returned[0] != "<START>");
	assert(returned[0] == "why");
	assert(returned[5] == "guy");
	if (returned[6] == "who") {
		assert(returned[7] == "knew");
	} else {
		assert(returned[6] == "s");
		assert(returned[8] == "<END>");
		cout << "YAHOO!" << endl;
	}
	
	
}

/*
//  semi for shiggles semi for testing vector shit 
void testPrintVector() {
	
	vector<string> expected = {"<START>", "laa", "dee", "daa", "<END>"};
	for(int i = 0; i < expected.size(); i++) {
		; 
	}
	
	
	
}*/

int main() {
	cout << "Testing read input..." << endl;
	testReadInput();
	cout << "Testing increment occur..." << endl;
	testIncrementOccur();
	cout << "Testing increment pair..." << endl;
	testIncrementPair();
	cout << "Testing incrementList..." << endl;
	testIncrementList();
	cout << "Testing LanguageModel Constructor..." << endl;
	testMakeLangMod();
	cout << "Testing word generator..." << endl;
	testGenWord();
	cout << "Testing text generator..." << endl;
	testGenText();
}
