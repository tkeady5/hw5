
#
#Thomas Keady and Nitin Kumar
#tkeady1 nkumar14
#516-729-9535
#248-797-2533
#tkeady1@jhu.edu
#nkumar14@jhu.edu
#4/1/15
#HW5
#

#Vars for compile

CXX = g++
CXXFLAGS = -std=c++11 -pedantic -Wall -Wextra -g

#functions

bin: hw5

	@echo "Running HW5:"
	./hw5

test: testMod

	@echo "Testing all functions..."
	./testMod
	@echo "Passed all tests..."

debugTest: testMod.cc langMod.cc

hw5: hw5.cc langMod.o

testMod: testMod.cc langMod.o

langMod.o: langMod.cc langMod.h

clean: 
	rm -rf *.o *.txt testMod hw5
