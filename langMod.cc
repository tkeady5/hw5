
/*
 * Thomas Keady and Nitin Kumar
 * tkeady1 nkumar14
 * 516-729-9535
 * 248-797-2533
 * tkeady1@jhu.edu
 * nkumar14@jhu.edu
 * 4/1/15
 * HW5
 */

#include <iostream>
#include <random>
//#include <vector>
//#include <string>

#include "langMod.h"

using std::cin;

//LanguageModel Functions

// Default contructor lets you fill in the maps later with other function calls
LanguageModel::LanguageModel() {
	
}

// This constructor does it for you
LanguageModel::LanguageModel(vector<string>& input) {
	for (vector<string>::iterator i = input.begin(); i != input.end(); i++) {
		incrementOccur(wordOccur, i);
		incrementPair(wordPair, input, i);
	}
	incrementList(wordList, input.begin(), input.end());
	
}


/*
 * Takes the created wordlist and the start/end iterators of the input vector, then 
 * creates linked lists for each word (ADDS <START>)
 */
void incrementList(unordered_map<string, list<string> >& wordList, vector<string>::iterator i, vector<string>::iterator end) {
	
	//int i = 1
	string lastWord = "<START>";	// Will always be like this
	i++;				// So get ready for the next one
	
	while (i != end) {				// While theres words in the vector,
		
		wordList[lastWord].push_front(*i);	// Add it to the list of lastWord
		wordList[lastWord].sort();		// Sort the list so
		wordList[lastWord].unique();		// you can use unique
		
		lastWord = *i;
		++i;
	}
	
}


/*
 * Increment occurrences of pairs
 */
void incrementPair (unordered_map<string, int>& wordPair, vector<string>& input, vector<string>::iterator iter) {
	if (iter == (input.end() - 1)) {			// Check if at last word 
		
	} else {						// Otherwise increment word pair
		string key = *iter + *(iter + 1);		// Find value in unordered map based on input string
		unordered_map<string, int>::iterator mapIter = wordPair.find(key);
				// If value doesn't exist, insert a single occurrence
		if ( mapIter == wordPair.end() ) {
			wordPair.insert({{key, 1}});
		} else {					// If value is found, increment iu
			mapIter -> second++;
		}
		
	}
	
}

/*
 * Increments single word occurances
 */
void incrementOccur (unordered_map<string, int>& wordOccur, vector<string>::iterator iter) {
	//Find value in unordered map based on input string
	unordered_map<string, int>::iterator mapIter = wordOccur.find(*iter);
	// If value doesn't exist, insert a single occurrence
	if ( mapIter == wordOccur.end() ) {
		wordOccur.insert({{*iter, 1}});
	}
	// If value is found, increment it
	else {
		mapIter -> second++;
	}
}


//Main program functions

/*
 *  * Reads from stdin to a vector it makes and returns
 *   * Vector of strings (repeats allowed)
 */
vector<string> readInput() {
	vector<string> vec(1, "<START>");
	//printf("%s\n", vec[0].c_str());;
	string holder = "";
	//int counter = 0;
	
	while (cin >> holder) {
		vec.push_back(holder);
	}
	vec.push_back("<END>");
	//printf("%s %s %s %s %s\n", vec[0].c_str(), vec[1].c_str(), vec[2].c_str(), vec[3].c_str(), vec[4].c_str());
	return vec;	
	
}

/*
 * Called from genText(), uses the language model to figure
 * out the next word in the sequence and return that
 * Takes int prob so that it can be manipulated for testing, and
 * can be called with the proper value during execution
 */

string genWord(LanguageModel& langModel, string prevWord, int occur) {

	//find a random number between 1 and that number
	std::default_random_engine generator(std::random_device{}());	    //  V THOGHT IF I MADE THIS 0 it would fix it (but it doesnt :'( )
	std::uniform_int_distribution<int> distribution(1,occur);
	int wordChoice = distribution(generator);	//generates number in range 1 - occur
	int sum = 0;
	//string to be used for holding concatenated string
	//to search wordPair
	string pair;
	//define iterator to scroll through linked list
	list<string>::iterator iter = langModel.wordList[prevWord].begin();
	pair = prevWord + *iter;
	sum += langModel.wordPair[pair];
	//once the sum is greater or equal than the
	//random number use the second word
	while(sum < wordChoice) {
		iter++;
		//grab second word from linked list
		//concatenate second word with prevword
		pair = prevWord + *iter;
		//see number of occurrences from wordPair
		//add to running sum (initialized at 0)
		sum += langModel.wordPair[pair];
	}
	return *iter;
}


/*
 * Takes the LanguageModel and uses the data
 * in the unordered_maps to make a sentence
 * according to the language model
 */
vector<string> genText(LanguageModel& langModel) {
	//define first word (start)
	string currWord = "<START>";
	//define return vector
	vector<string> retVector(1, currWord);
	//retVector[0] = currWord;
	//repeat till grabbed word has an empty associated linked list
	//for(int i = 1; langModel.wordList[currWord] != std::list<string>(); i++) {
	for (int i = 1; currWord != "<END>"; ++i) {
		//find number of occurences of word and pass to genWord
		currWord = genWord(langModel, currWord, langModel.wordOccur[currWord]);
		//retVector
		//retVector[i] = currWord;
		retVector.push_back(currWord);
	}
	
	return retVector;

}



/*
 * Prints the contents of a vector
 */
void printVector(vector<string> input) {
	
	for (unsigned int i = 1; i < input.size() - 1; ++i) {
		std::cout << input[i].c_str() << " ";		// Print each word in the vector
	}
	std::cout << std::endl;

}

/*
 * Prints the contents of a vector with the <START> and <END> strings
 * Could be achieved with another function, but I thought I'd try my
 * hand at overloading, so its overloaded 
 */
void printVector(vector<string> input, bool printEnds) {
	//for (input::iterator i = input.begin(); i != input.end(); ++i) {
	
	if (printEnds) {
		for (unsigned int i = 0; i < input.size(); ++i) {
			printf("%s ", input[i].c_str());	// Another way of doing things
		}
		printf("\n");
	}
	
}

