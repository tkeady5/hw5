/*
 * Thomas Keady and Nitin Kumar
 * tkeady1 nkumar14
 * 516-729-9535
 * 248-797-2533
 * tkeady1@jhu.edu
 * nkumar14@jhu.edu
 * 4/1/15
 * HW5
 */



#ifndef _LANGMOD_H_INCLUDED_
#define _LANGMOD_H_INCLUDED_


#include <vector>
#include <string>
#include <list>
#include <unordered_map>

using std::vector;
using std::unordered_map;
using std::string;
using std::list;

struct LanguageModel {
	unordered_map<string, list<string>> wordList;	// Holds individual words and the
							// linked list of subsequent words
	
	unordered_map<string, int> wordOccur; 		// Holds the number of occurances of
							// a given word
	
	unordered_map<string, int> wordPair;		// Holds each word pair and the number
							// of times it occurs


	/*
	 * Take the vector of words made by readInput()
	 * and make the 3 unordered_maps 
	 * Return LanguageModel struct with those maps
	 */
	LanguageModel();
	LanguageModel(vector<string>& input);

};

//Add to the number of times a words has occurred value
//in the unordered map
//		TAKES TWO ITERATORS
void incrementList(unordered_map<string, list<string>>& wordList, vector<string>::iterator i, vector<string>::iterator end);

//Add the next word linked list correlated to a certain word
void incrementOccur(unordered_map<string, int>& wordOccur, vector<string>::iterator i);

//incremement occurences of combination of first word, next word
void incrementPair(unordered_map<string, int>& wordPair, vector<string>& input, vector<string>::iterator iter);


/*
 * Reads from stdin to a vector it makes and returns
 * Vector of strings (repeats allowed)
 */
vector<string> readInput();



/*
 * Called from genText(), uses the language model to figure
 * out the next word in the sequence and return that
 * Takes int prob so that it can be manipulated for testing, and
 * can be called with the proper value during execution
 */
string genWord(LanguageModel& langModel, string prevWord, int occur);




/*
 * Takes the LanguageModel and uses the data
 * in the unordered_maps to make a sentence
 * according to the language model
 */
vector<string> genText(LanguageModel& langModel);



/*
 * Prints the contents of a vector
 */
void printVector(vector<string> input);




/*
 * Prints the contents of a vector with the <START> and <END> strings
 * Could be achieved with another function, but I thought I'd try my
 * hand at overloading, so its overloaded 
 */
void printVector(vector<string> input, bool printEnds);



#endif
